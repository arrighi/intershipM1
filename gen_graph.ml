open Printf;;

module Threefold =
  struct
    type t = int * int * int;;

    let compare = Pervasives.compare;;

    let make x y z =
      let x, y = if x < y then x, y else y, x in
      let y, z = if y < z then y, z else z, y in
      let x, y = if x < y then x, y else y, x in
      x,y,z;;

    let fst (x,y,z) = x;;
    let snd (x,y,z) = y;;
    let trd (x,y,z) = z;;
  end

module Pair =
  struct
    type t = int * int;;

    let compare = Pervasives.compare;;

    let make x y = if x < y then x, y else y, x;;
  end

type hypergraph = {v: int; mutable he: (int * int * int) list};;
type graph = {v: int; mutable e: (int*int) list};;

(* Debug function : print list of pair and threefold *)
let rec print_list_threefold = function
  | [] -> ()
  | (x,y,z)::l -> begin
      print_string "("; print_int x ; print_string ", "; print_int y; print_string ", ";
      print_int z; print_string "); "; print_list_threefold l;
    end
;;

let rec print_list_pair = function
  | [] -> ()
  | (x,y)::l -> begin
      print_string "("; print_int x ; print_string ", "; print_int y;  print_string "); "; print_list_pair l;
    end
;;

(* Naive methode, toss a coin for each hyperedges *)
let gen_hyp n l =
  (* Initialisation of random with /dev/urandom *)
  Random.self_init ();
  let epsilon = Random.float (1. /. (2. *. l +. 3.)) in
  let p = (float_of_int n) ** (epsilon -. 2.) in
  let el = ref [] in

  (* Hypergraph random generation *)
  for i = 1 to n do
    for j = i+1 to n do
      for k = j+1 to n do
        if Random.float 1. <= p then el := (Threefold.make i j k) ::(!el);
      done;
    done;
  done;
  !el
;;

(* Random graph with p = 1/2 *)
let random_graph n =
  (* Initialisation of random with /dev/urandom *)
  Random.self_init ();
  let l = ref [] in
  for i = 1 to n do
    for j = i+1 to n do
      if Random.bool () then l := (Pair.make i j)::(!l);
    done;
  done;
  {v = n; e = !l}
;;

(* Compute the probability to have k hyperedge in the graph *)
let binomialLaw prob n k =
  let exp = (n -. k) /. k in
  let rec cm res num denum =
    if denum >= 1. then cm ((res *. num *. prob *. ((1. -. prob) ** exp)) /. denum) (num -. 1.) (denum -. 1.)
    else res in
  if k > 0. then
    cm 1. n k
  else (1. -. prob) ** n
;;

(* Chose at random the number of hyperedge in the graph *)
let nb_edges nb_vertex p =
  let n = float_of_int (nb_vertex * (nb_vertex-1) * (nb_vertex-1) / 6)in
  let pp = ref (binomialLaw p n 0.) in
  let k = ref 0 in
  Random.self_init ();
  let r = Random.float 1. in
  while !pp < r do
    k := !k + 1;
    pp := !pp +. (binomialLaw p n (float_of_int !k));
  done;
  !k
;;


(* Generate random hypergraph by first choose at random the number of hyperedge
 * and take in an uniforme way this number of hyperedge *)
let quick_gen_hyp n l =
  (* Initialisation for Random with /dev/urandom *)
  Random.self_init ();
  let epsilon = (0.99 /. (2. *. l +. 3.)) in
  let p = (float_of_int n) ** (epsilon -. 2.) in
  let ne = nb_edges n p in
  let ae = Array.make n 0 in
  let rec gen el = function
    | 0 -> el
    | i -> begin
      let e = ref (Threefold.make (Random.int n) (Random.int n) (Random.int n)) in
      while List.mem (!e) el || Threefold.fst (!e) = Threefold.snd (!e) || Threefold.snd (!e) = Threefold.trd (!e) || Threefold.fst (!e) = Threefold.trd (!e) do
        e := (Threefold.make (Random.int n) (Random.int n) (Random.int n));
      done;
      ae.(Threefold.fst (!e)) <- ae.(Threefold.fst (!e)) + 1;
      ae.(Threefold.snd (!e)) <- ae.(Threefold.snd (!e)) + 1;
      ae.(Threefold.trd (!e)) <- ae.(Threefold.trd (!e)) + 1;
      gen ((!e)::el) (i-1);
    end;
  in
  (gen [] ne),ae
;;


(* Generate the feet hypergraph with 2n vertex from a hypergraph (first step of
 * the construction of a multipede)
 * for all vertex i, 2i and 2i+1 are the associate feet
 * and for all hyperedge, four hyperedges are build to verify the property of a
 * 2-multipede*)
let gen_feet_hg n el =
  let rec aux l = function
    | [] -> l
    | (x,y,z)::t -> begin
        if Random.bool () then
          aux ((2*x, 2*y, 2*z)::(2*x, 2*y+1, 2*z+1)::(2*x+1, 2*y+1,2*z)::(2*x+1, 2*y, 2*z+1)::l) t
        else
          aux ((2*x+1, 2*y, 2*z)::(2*x, 2*y, 2*z+1)::(2*x, 2*y+1,2*z)::(2*x+1, 2*y+1, 2*z+1)::l) t
      end
  in
  {v = 2*n; he = aux [] el}
;;

(* Convert the feet hypergraph into a undirected graph with an implice
 * linear order on the segment hypergraph*)
let feet_hg_to_graph en el =

  if en mod 2 != 0 then failwith "wrong graph, n must be even";
  (* For all the pair of feet, 5 vertex are generated to simulate oriented edge
   * for the linear order *)
  let rec linear_order l = function
    | -2 -> l
    | n -> begin
        let l1 = (n,n+2)::(n,n+3)::(n+1,n+2)::(n+1,n+3)::l in
    manager.shutdown()
    print("Server stoped.", file=sys.stderr)
        linear_order l1 (n-2)
      end
  in

  (* All hyperedge are encoded by a extra vertex *)
  let rec hyperedge l n = function
    | [] -> {v = n; e = List.sort compare l};
    | (a,b,c)::t -> begin
        hyperedge ((a,n)::(b,n)::(c,n)::l) (n+1) t
      end
  in
  (* Structure to force a orientation of the chaine *)
  let l1 = (0,en)::(1,en)::(en-2,en+2)::(en-1,en+2)::(en,en+1)::(en,en+2)::(en+2,en+3)::(en+3,en+4)::[] in
  hyperedge ((linear_order l1 (en-4))) (en + 5) el
;;

(* Write the graph g in fout *)
let write_graph fout g =
  fprintf fout "p edge %i %i" g.v (List.length g.e);
  List.iter (fun (x,y) -> fprintf fout "\ne %i %i" (x+1) (y+1);) g.e;
;;


(*Commande line option*)
let uniform_graph = ref false;;
let rand_struct = ref false;;
let vertex_num = ref 10;;
let l = ref 2.;;
let fout = ref "";;

let speclist = [("-r", Arg.Set uniform_graph, "Generate of random graph with p = 1/2");
                ("-s", Arg.Set rand_struct, "Generate random graph based on 3-multipede");
                ("-n", Arg.Set_int vertex_num,"Number of vertex for the graph generation (for the 3 multipede is the number of vertex of the hypergraph. Default : 10)");
                ("-l", Arg.Set_float l,"Set the number l for the generation of the 3-multipede. Default : 2.)");
                ("-o", Arg.Set_string fout,"Output file. Default : stdout")
               ];;
let usage_msg = "Generate graph with different methode. Options available:";;
Arg.parse speclist print_endline usage_msg;;

let output = if !fout = "" then stdout else open_out !fout;;

if !uniform_graph then write_graph output (random_graph !vertex_num)
else if !rand_struct then begin
  let el,_ = (quick_gen_hyp !vertex_num !l) in
  let a = gen_feet_hg !vertex_num el in
  write_graph output (feet_hg_to_graph a.v a.he)
end
else Arg.usage speclist usage_msg;;
