#!/usr/bin/env python3
"""
Convert graph file in Bliss format to be use with dreadnaut
Usage: parser.py -o output input
"""

import argparse, sys, os
import string

def convert(fi, fo):
    """ Convert the input file into the output file """
    line = fi.readline()
    dec_line = line.split(" ")
    if line.find("p edge") != 0 or len(dec_line) != 4:
        print('Error wrong format: "{}"'.format(line))
        sys.exit(2)
    nb_vertex = int(dec_line[2])
    fo.write("n={} g\n".format(nb_vertex))
    vertex = 0
    fo.write("0 : ")
    for l in fi:
        dec_l = l.rstrip('\n').split(" ")
        if not (len(dec_l) == 3 and dec_l[1].isdigit() and dec_l[2].isdigit()):
            print(dec_l)
            print('Error wrong format: "{}"'.format(l))
            sys.exit(2)
        if int(dec_l[1]) -1 == vertex:
            fo.write(" {}".format(int(dec_l[2]) -1))
        else:
            vertex = int(dec_l[1]) -1
            fo.write("\n{} :  {}".format(vertex, int(dec_l[2]) -1))
    for i in range(vertex +1,nb_vertex):
        fo.write("\n{} : ".format(i))
        fo.write("\n")
    fo.write(".\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert graph file")
    parser.add_argument("-i", "--input", type=argparse.FileType('r'), default=sys.stdin, help="input file, default : read graph from stdin")
    parser.add_argument("-o", "--output", type=argparse.FileType('w'), default=sys.stdout, help="Name of the output file. Default: wirte on stdout")
    args = parser.parse_args()
    convert(args.input, args.output)
