The fastest practical algorithms for solving the graph isomorphism
problem are nauty and Traces.  You can find details about these at:
http://pallini.di.uniroma1.it

At that link you will also find links to papers by Brendan McKay and
Adolfo Piperno which explain the design of the system.  You may want to
read this.  You might also look at how to get the system installed and
up and running - which is something you will need to do when you are here.

Now, my idea is to construct examples of graphs which are hard for these
systems.  The systems work by distinguishing vertices in the graph and
where they fail, finding automorphisms with which to factor them.  I
believe we can construct graphs that at the same time (1) have vertices
that are difficult to distinguish and (2) have no automorphisms.  The
construction is based on the odd multipedes in this paper:
http://research.microsoft.com/en-us/um/people/gurevich/Opera/113.pdf

Now, the construction there doesn't yield graphs directly, but this is
easily fixed.  Also, it is probabilistic, so some search will be needed
to get concrete examples.  You'll need to develop a program for doing so.

The final part of the project will be to set up some experiments to see
how nauty/Traces performs on the examples constructed, and perhaps
compare this against some known benchmarks.
