Script for Trace benchmark
==========================

 * *gen_graph.ml*: generation of graph
 * *C411.sh*: Do the computation on the computer in the C411
 * *benchmark.py*: make statistics on Trace over some sample
 * *trace_bench.c*: take a graph and print the time of the computation of the canonical labeling

Installation
============

 * Clone the git repository
 * Download and install the last version of Naucy & Trace : http://pallini.di.uniroma1.it/
 * Compile trace_bench.c in trace_bench with nauty.a
 * Place trace_bench in the same folder as benchmark.py

How to use benchmark.py
=======================

See benchmark.py -h
