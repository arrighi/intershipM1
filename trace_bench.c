/* This program calculate how long canonical labeling take
    using Trace on the given graph.
*/

#define HELPTEXT \
" benchmark [-h] [-f infile]\n\
\n\
Read files of graphs in Bliss (Dimacs) format and \n\
mesure the time to calculate canonical labeling on this \n\
graph using Trace.\n\
\n\
   -f infile : Input file, otherwise read the graph on stdin\n"

#define _GNU_SOURCE
#include "traces.h"
#include "naututil.h"
#include <sys/time.h>
#include <sys/resource.h>

typedef struct
{
   int v,w;
} vpair;

static int
nextchar(FILE *f)
{
    char s[2];

    if (fscanf(f,"%1s",s) != 1) return EOF;
    else                        return s[0];
}

static boolean
readblissgraph(FILE *f, sparsegraph *g)
/* Reads a graph from Bliss format into a sparse graph */
{
    int n,c;
    unsigned long ne,j;
    int haven;
    int i,v,w;
    int haveptn;
    DYNALLSTAT(vpair,elist,elist_sz);

    haven = 0;
    j = 0;
    while ((c = nextchar(f)) >= 0)
    {
    switch (c)
    {
    case 'c':
        while ((c = getc(f)) != '\n' && c != EOF) {}
        break;

    case 'p':
        if (haven)
        {
        fprintf(stderr,"Duplicate p line\n");
        exit(1);
        }
        if (fscanf(f," edge %d %lu",&n,&ne) != 2)
        {
        fprintf(stderr,"Bad p line\n");
        return FALSE;
        }
        haven = 1;
            DYNALLOC1(vpair,elist,elist_sz,ne,"Alloc vpair");
        break;

    case 'n':
        if (!haven)
        {
                fprintf(stderr,"Missing p line\n");
                return FALSE;
            }
            if (fscanf(f,"%d%d",&w,&v) != 2 || w < 1 || w > n)
            {
                fprintf(stderr,"Bad n line\n");
                return FALSE;
            }
        break;

    case 'e':
        if (!haven || j == ne)
        {
        fprintf(stderr,"Missing p line or too many e lines\n");
        return FALSE;
        }
        if (fscanf(f,"%d%d",&v,&w) != 2 || v < 1 || w < 1 || v > n || w > n)
        {
        fprintf(stderr,"Bad e line\n");
        return FALSE;
        }
        elist[j].v = v-1; elist[j].w = w-1;
        ++j;
        break;

    default:
        fprintf(stderr,"Unknown line %c\n",c);
        return FALSE;
    }
    }

    if (j != ne)
    {
        fprintf(stderr,"Wrong number of e lines\n");
        exit(1);
    }

    SG_ALLOC(*g,n,2*ne,"SG_ALLOC");
    g->nv = n;
    g->nde = 2*ne;

    for (i = 0; i < n; ++i) g->d[i] = 0;
    for (j = 0; j < ne; ++j)
    {
    ++(g->d[elist[j].v]);
    ++(g->d[elist[j].w]);
    }
    g->v[0] = 0;
    for (i = 1; i < n; ++i) g->v[i] = g->v[i-1] + g->d[i-1];
    for (i = 0; i < n; ++i) g->d[i] = 0;

    for (j = 0; j < ne; ++j)
    {
    v = elist[j].v;
    w = elist[j].w;
        g->e[g->v[v]+(g->d[v])++] = w;
        g->e[g->v[w]+(g->d[w])++] = v;
    }

    return TRUE;
}

int main(int argc, char *argv[])
{
    DYNALLSTAT(int,lab,lab_sz);
    DYNALLSTAT(int,ptn,ptn_sz);
    DYNALLSTAT(int,orbits,orbits_sz);
    static DEFAULTOPTIONS_TRACES(options);
    TracesStats stats;
 /* Declare and initialize sparse graph structures */
    SG_DECL(sg);
    SG_DECL(cg);

    ssize_t len = 0;
    size_t lfg = 0;

    /*Measure time to compute the canonical labelling*/
    struct rusage start_t, end_t;
    struct timeval user_time;

 /* Select option for canonical labelling */

    options.getcanon = TRUE;

/* Option selection */
    if (argc >= 3 && argv[1][0] == '-' && argv[1][1] == 'f')
    {
        FILE * fg;

        if((fg = fopen(argv[2], "r")) == NULL)
        {
            fprintf(stderr, "Error while opening file : %s\n", argv[2]);
            exit(1);
        }
        readblissgraph(fg, &sg);
        fclose(fg);
    }
    else if (argc >= 2 && argv[1][0] == '-' && argv[1][1] == 'h')
        {
            fprintf(stdout, HELPTEXT);
            exit(0);
        }
    else if(argc == 1)
        {readblissgraph(stdin, &sg);}
    else
    {
        fprintf(stderr, HELPTEXT);
        exit(1);
    }

    nauty_check(WORDSIZE,1,sg.nv,NAUTYVERSIONID);

    /*Dynamic allocation*/
    DYNALLOC1(int,lab,lab_sz,sg.nv,"malloc");
    DYNALLOC1(int,ptn,ptn_sz,sg.nv,"malloc");
    DYNALLOC1(int,orbits,orbits_sz,sg.nv,"malloc");

    if(getrusage(RUSAGE_SELF, &start_t) != 0)
    {
        fprintf(stderr, "Error when reading time.");
        exit(1);
    }
    Traces(&sg,lab,ptn,orbits,&options,&stats,&cg);
    if(getrusage(RUSAGE_SELF, &end_t) != 0)
    {
        fprintf(stderr, "Error when reading time.");
        exit(1);
    }
    timersub(&end_t.ru_utime, &start_t.ru_utime, &user_time);
    printf("number of vertices : %d ,", sg.nv);
    printf("execution time : %ld.%06ld\n", (long int) user_time.tv_sec, (long int) user_time.tv_usec);

    exit(EXIT_SUCCESS);
}
