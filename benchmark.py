#!/usr/bin/python3
# -*- mode: python; coding: utf-8 -*-

import os, sys, subprocess, time, signal
import argparse
from multiprocessing import Queue, Process, managers, Manager
from queue import Empty
from random import shuffle

from time import sleep

# Handle the sig, to end properly and save the result before dying
def sigterm_handler(_signo, _stack_frame):
    # Raises SystemExit(0):
    sys.exit(0)

def bench(f):
    """
        Start trace_bench on file f in a subprocess and return the result strinrg
    """
    try:
        benchmark = subprocess.check_output(["./trace_bench", "-f", "%s" % f], universal_newlines = True)
        return benchmark[:-1]
    except subprocess.CalledProcessError:
        print("Error with %s"%f, file=sys.stderr)


def find_file(path_dir):
    """
        Return the list of path of file that are in the directory and its subdirectory
    """
    file_list = []
    for directory in os.walk(path_dir):
        for f in directory[2]:
            file_list.append(os.path.abspath(directory[0] + "/" + f))
    return file_list


def check_file(l):
    """ Take a list of path and return the list of path that correspond to
        a real file
    """
    file_list = []
    for f in l:
        if os.path.isfile(f):
            file_list.append(f)
        else:
            print("Error with %s" % f, file = sys.stderr)
    return file_list


def trace_workers(graph_q, result_q):
    """ A worker function to be launched in a separate process. Takes graph from
        graph_q and compute the canonical labeling with Trace. When this is done,
        the result (number of vertex, computation time) is placed into
        result_q. Runs until graph_q is empty.
    """
    while True:
        try:
            graph = graph_q.get_nowait()
            res = bench(graph)
            if res is not None:
                result_q.put((int(res.split()[4]), float(res.split()[8]), graph))
        except Empty:
            return

def mp_trace(share_graph_q, share_result_q, nprocs):
    """ Split the work with graph in shared_graph_q and results in
        shared_result_q into several processes. Launch each process with
        trace_worker as the worker function, and wait until all are
        finished.
    """
    if nprocs == 1:
        trace_workers(share_graph_q, share_result_q)
    else:
        procs = []
        for i in range(nprocs):
            p = Process(target=trace_workers,
                    args=(share_graph_q, share_result_q))
            procs.append(p)
            p.start()
        for p in procs:
            p.join()

def make_server_manager(port, authkey):
    """ Create a manager for the server, listening on the given port.
        Return a manager object with get_graph_q and get_result_q methods.
    """
    graph_q = Queue()
    result_q = Queue()

    class GraphQueueManager(managers.SyncManager):
        pass

    GraphQueueManager.register('get_graph_q', callable=lambda: graph_q)
    GraphQueueManager.register('get_result_q', callable=lambda: result_q)

    manager = GraphQueueManager(address=('', port), authkey=authkey)
    manager.start()
    print('Server started at port %s' % port, file= sys.stderr)
    return manager

def runserver(portnum, authkey, file_list, fout):
    """
        Start a server, fill the queue with the list of file, receive the value
        of trace.
        Save the value obtient by Trace in fout.
        Return the value obtient by Trace in a dic.
    """
    # Start a shared manager server and access its queues
    manager = make_server_manager(portnum, authkey)
    shared_graph_q = manager.get_graph_q()
    shared_result_q = manager.get_result_q()

    N = len(file_list)

    # Files of graph are pushed into the graph queue.
    for f in file_list:
        shared_graph_q.put(f)

    # Wait until all results are ready in shared_result_q
    numresults = 0
    resultdict = {}
    try:
        while numresults < N:
            res = shared_result_q.get()
            if res[0] in resultdict:
                resultdict[res[0]].append((res[1], res[2]))
            else:
                resultdict[res[0]] = [(res[1],res[2])]
            numresults += 1
        # Sleep a bit before shutting down the server - to give clients time to
        # realize the graph queue is empty and exit in an orderly way.
        time.sleep(2)

    # Capture the ctrl-C signal to allow user to stop the server without all the
    # result (if a client crash)
    finally:
        manager.shutdown()
        save_result(resultdict,ARGS.output)
        print("Server stoped.", file=sys.stderr)

        return resultdict


def make_client_manager(ip, port, authkey):
    """ Create a manager for a client. This manager connects to a server on the
        given address and exposes the get_graph_q and get_result_q methods for
        accessing the shared queues from the server.
        Return a manager object.
    """
    class ServerQueueManager(managers.SyncManager):
        pass

    ServerQueueManager.register('get_graph_q')
    ServerQueueManager.register('get_result_q')

    manager = ServerQueueManager(address=(ip, port), authkey=authkey)
    manager.connect()

    print('Client connected to %s:%s' % (ip, port))
    return manager


def runclient(ip, portnum, authkey, nproc):
    """
        Creater a client manager. Receive the shared queue for graph and result.
        And call mp_trace to do the calculation.
    """
    manager = make_client_manager(ip, portnum, authkey)
    graph_q = manager.get_graph_q()
    result_q = manager.get_result_q()
    mp_trace(graph_q, result_q, nproc)
    print("Client stoped.")

def save_result(resultdict, fout):
    """
        Take a dic of result and save it in fout.
    """
    fout.write("==== Summarized Data ====\n\n")
    resultlist = sorted(list(resultdict.items()))
    for n,l in resultlist:
        average = 0
        maximum = l[0][0]
        maxfile = l[0][1]
        for t,f in l:
            if t > maximum:
                maximum = t
                maxfile = f
            average += t
        average /= len(l)
        fout.write("vertex number: %d, average time in seconde: %.24f, max time: %.24f for %s\n" % (n, average, maximum, maxfile))
    fout.write("\n\n==== Raw Data ====\n\n")
    for n,l in resultlist:
        for t,f in l:
            fout.write("vertex number: %d, time: %.24f, file: %s\n" % (n,t,f))


if __name__ == "__main__":

    # Parser option
    PARSER = argparse.ArgumentParser(description="Make statistics about Trace on graphs in Bliss format")
    PARSER.add_argument("-o", "--output", type=argparse.FileType('w'), default=sys.stdout, help="Output file")

    INPUT_GROUP = PARSER.add_mutually_exclusive_group(required=True)
    INPUT_GROUP.add_argument("-d", "--dir", type=str, help="Directory of graphs")
    INPUT_GROUP.add_argument("-f", "--file", type=str, nargs="+", help="List of graph files")
    INPUT_GROUP.add_argument("-c", "--client", type=str, metavar="IP", help="Connect to the server with the IP and start the computation.")

    PARSER.add_argument("-s", "--server", action="store_true", help="Set up the server to use multiple computer.")

    PARSER.add_argument("-p", "--port", type=int, default=2121, help="Port to use for server and client.")
    PARSER.add_argument("-k", "--key", type=str, default='auie', help="Authentification key to use for server and client connexion.")
    PARSER.add_argument("-i", "--iter", type=int, default=1, help="Benchmark each graph ITER time.")

    PARSER.add_argument("-m", "--multicore", type=int, default=1, choices=[i+1 for i in range(os.cpu_count())], help="Number of process to use for the computation. Valid for the client and simple computation.")
    ARGS = PARSER.parse_args()

    file_list = []

    signal.signal(signal.SIGTERM, sigterm_handler)

    if ARGS.dir is not None:
        if not os.path.isdir(ARGS.dir):
            print("Error: %s is not a directory" % ARGS.dir, file=sys.stderr)
            sys.exit(1)
        tmp_list = find_file(ARGS.dir)
        for i in range(ARGS.iter):
            file_list += tmp_list
        shuffle(file_list)
    if ARGS.file is not None:
        tmp_list = check_file(ARGS.file)
        for i in range(ARGS.iter):
            file_list += tmp_list
        shuffle(file_list)
    if ARGS.server:
        runserver(ARGS.port, ARGS.key.encode(sys.getdefaultencoding()), file_list, ARGS.output)
    elif ARGS.client:
        runclient(ARGS.client, ARGS.port, ARGS.key.encode(sys.getdefaultencoding()), ARGS.multicore)
    else:
        resultdict = {}
        try:
            if ARGS.multicore == 1:
                for f in file_list:
                    res = bench(f).split()
                    if int(res[4]) in resultdict:
                        resultdict[int(res[4])].append((float(res[8]),f))
                    else:
                        resultdict[int(res[4])] = [(float(res[8]),f)]
            else:
                manager = Manager()
                graph_q = manager.Queue()
                result_q = manager.Queue()

                for f in file_list:
                    graph_q.put(f)

                mp_trace(graph_q, result_q, ARGS.multicore)
                while not result_q.empty():
                    res = result_q.get()
                    if res[0] in resultdict:
                        resultdict[res[0]].append((res[1],res[2]))
                    else:
                        resultdict[res[0]] = [(res[1],res[2])]

            sleep(20)
        finally:
            print("Ending programme, saving result.", file=sys.stderr)
            save_result(resultdict,ARGS.output)
